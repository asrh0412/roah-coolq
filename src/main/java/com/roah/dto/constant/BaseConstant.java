package com.roah.dto.constant;

import java.util.logging.Level;

/**
 * 基础配置信息
 *
 * @author Roah
 * @since 01/06/2019
 */
public class BaseConstant {
    public static final String TURL = "";
    public static final String packageName = "com.roah";
    public static final String fileName = "test.log";
    public static final Level logLevel = Level.ALL;
    public static final String URL = "http://openapi.tuling123.com/openapi/api/v2";
    public static final String QQName = "AI-小阮阮";
    public static String appId = "037f96427fbb4251a542ddbb63fd20b1";
    public static final String MyName = "小阮阮又瘦了";
    public static final String DefaultQQ = "2690394124";
    public static final String DefaultMQConf = "/mqconfig";

}
