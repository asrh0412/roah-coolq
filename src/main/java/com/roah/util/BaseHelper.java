package com.roah.util;

import com.alibaba.fastjson.JSONObject;
import com.roah.dto.constant.BaseConstant;

/**
 * TODO: Add Description
 *
 * @author Roah
 * @since 01/07/2019
 */
public class BaseHelper {
    public static JSONObject getJsonRes(String msg,long fromQQ){
        JSONObject testJson = new JSONObject();
        JSONObject perception = new JSONObject();
        JSONObject inputText = new JSONObject();
        JSONObject userInfo = new JSONObject();
        Long from = Long.valueOf(fromQQ);
        inputText.put("text",msg);
        userInfo.put("apiKey", BaseConstant.appId);
        userInfo.put("userId",from.toString());
        perception.put("inputText",inputText);
        testJson.put("perception",perception);
        testJson.put("reqType",0);
        testJson.put("userInfo",userInfo);
        return testJson;
    }

}
