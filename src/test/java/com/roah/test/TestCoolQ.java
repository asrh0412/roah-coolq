package com.roah.test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.roah.Demo;
import com.roah.dto.constant.BaseConstant;
import com.roah.util.BaseHelper;
import com.roah.util.HttpHelper;
import com.roah.util.HttpUtil;
import com.roah.util.TalkUtil;
import com.sobte.cqp.jcq.entity.CQDebug;
import org.junit.Test;

import static com.sobte.cqp.jcq.event.JcqApp.CQ;

/**
 * 单元测试
 *
 * @author Roah
 * @since 01/06/2019
 */
public class TestCoolQ {
    /**
     *
     * 主流程测试
     */
    @Test
    public void MainTest(){
        // CQ此变量为特殊变量，在JCQ启动时实例化赋值给每个插件，而在测试中可以用CQDebug类来代替他
        CQ = new CQDebug();//new CQDebug("应用目录","应用名称") 可以用此构造器初始化应用的目录
        CQ.logInfo("[JCQ] TEST Demo", "测试启动");// 现在就可以用CQ变量来执行任何想要的操作了
        // 要测试主类就先实例化一个主类对象
        Demo demo = new Demo();
        // 下面对主类进行各方法测试,按照JCQ运行过程，模拟实际情况
        demo.startup();// 程序运行开始 调用应用初始化方法
        demo.enable();// 程序初始化完成后，启用应用，让应用正常工作
        // 开始模拟发送消息
        // 模拟私聊消息
        // 开始模拟QQ用户发送消息，以下QQ全部编造，请勿添加
        //demo.privateMsg(0, 10005, 3333333334L, "你好坏，都不理我QAQ", 0);
        // 模拟群聊消息
        // 开始模拟群聊消息
        demo.groupMsg(0, 1278457484, 3456789012L, 3333333334L, "", "@AI-小阮阮 上床吧", 0);
        // ......
        // 依次类推，可以根据实际情况修改参数，和方法测试效果
        // 以下是收尾触发函数
        // demo.disable();// 实际过程中程序结束不会触发disable，只有用户关闭了此插件才会触发
        demo.exit();// 最后程序运行结束，调用exit方法
    }
    /**
     *
     * 讨论组测试
     */
    @Test
    public void testDiscuss(){
        // CQ此变量为特殊变量，在JCQ启动时实例化赋值给每个插件，而在测试中可以用CQDebug类来代替他
        CQ = new CQDebug();//new CQDebug("应用目录","应用名称") 可以用此构造器初始化应用的目录
        CQ.logInfo("[JCQ] TEST Demo", "测试启动");// 现在就可以用CQ变量来执行任何想要的操作了
        // 要测试主类就先实例化一个主类对象
        Demo demo = new Demo();
        // 下面对主类进行各方法测试,按照JCQ运行过程，模拟实际情况
        demo.startup();// 程序运行开始 调用应用初始化方法
        demo.enable();// 程序初始化完成后，启用应用，让应用正常工作
        // 开始模拟发送消息
        // 模拟私聊消息
        // 开始模拟QQ用户发送消息，以下QQ全部编造，请勿添加
        //demo.privateMsg(0, 10005, 3333333334L, "你好坏，都不理我QAQ", 0);
        demo.discussMsg(0,10006,123,123,"我的天哪",0);
        // 模拟群聊消息
        // 开始模拟群聊消息
        //demo.discussMsg();
        // ......
        // 依次类推，可以根据实际情况修改参数，和方法测试效果
        // 以下是收尾触发函数
        // demo.disable();// 实际过程中程序结束不会触发disable，只有用户关闭了此插件才会触发
        demo.exit();// 最后程序运行结束，调用exit方法

    }
    /**
     *
     * get请求测试
     */
    @Test
    public void testGet(){

    }
    /**
     *
     * Post请求自动回复测试
     */
    @Test
    public void testPost(){
        String msg1 = "@AI-小阮阮 北京最好玩的酒吧";
        if(TalkUtil.match(msg1,"@"+ BaseConstant.QQName)){
            JSONObject testJson = BaseHelper.getJsonRes(msg1,1278457484);
            System.out.println(testJson.toJSONString());
            try {
                JSONObject resJson = HttpHelper.doPost(BaseConstant.URL,testJson);
                JSONArray resList = resJson.getJSONArray("results");
                for(Object test:resList){
                    JSONObject res = (JSONObject) test;
                    System.out.println("结果:"+res.getJSONObject("values").get("text"));
                }
            } catch (Exception e) {
                System.out.println(e);
                System.out.println("AI出问题了，请找我的爸爸@小阮阮又瘦了");
            }

        }else{
            System.out.println("不好使");
        }

    }
    @Test
    public void testRobo(){

    }
}
